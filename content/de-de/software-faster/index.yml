---
  title:  Software. Schneller
  description: GitLab vereinfacht Ihre DevSecOps, damit Sie sich auf das Wesentliche konzentrieren können. Erfahren Sie mehr!
  image_title: /nuxt-images/open-graph/open-graph-gitlab.png
  twitter_image: /nuxt-images/open-graph/open-graph-gitlab.png
  hero:
    title: |
      Der schnellere Weg von der Idee zur Software
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 0
    image:
      url: /nuxt-images/software-faster/hero.png
      alt: software-faster hero image
      aos_animation: fade-up
      aos_duration: 1600
      aos_offset: 0
    secondary_button:
      video_url: https://player.vimeo.com/video/799236905?h=59b06b0e99&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
      text: Was ist GitLab?
      data_ga_name: watch video
      data_ga_location: hero
  customer_logos:
    showcased_enterprises:
      - image_url: "/nuxt-images/home/logo_tmobile_mono.svg"
        link_label: Link zur Webcast-Landingpage von T-Mobile und GitLab
        alt: "T-Mobile logo"
        url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: Lien vers l'étude de cas client de Goldman Sachs
        alt: "Goldman Sachs logo"
        url: "/customers/goldman-sachs/"
      - image_url: "/nuxt-images/home/logo_cncf_mono.svg"
        link_label: Link zur Kundenfallstudie der Cloud Native Computing Foundation
        alt: "Cloud Native logo"
        url: /customers/cncf/
      - image_url: "/nuxt-images/home/logo_siemens_mono.svg"
        link_label: Link zur Kundenfallstudie von Siemens
        alt: "Siemens logo"
        url: /customers/siemens/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: Link zur Nvidia-Kundenfallstudie
        alt: "Nvidia logo"
        url: /customers/nvidia/
      - alt: UBS Logo
        image_url: "/nuxt-images/home/logo_ubs_mono.svg"
        url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
        link_label: Link zur Kundenfallstudie von UBS
  featured_content:
    col_size: 4
    header: "Gemeinsam besser: Kunden versenden Software schneller mit GitLab"
    case_studies:
      - header: Nasdaqs schneller, nahtloser Übergang in die Cloud
        description: |
          Nasdaq hat die Vision, zu 100 % Cloud zu werden. Sie arbeiten mit GitLab zusammen, um dorthin zu gelangen.
        showcase_img:
          url: /nuxt-images/software-faster/nasdaq-showcase.png
          alt: Nasdaq logo on a window
        logo_img:
          url: /nuxt-images/enterprise/logo-nasdaq.svg
          alt: Nasdaq logo
        link:
          video_url: https://player.vimeo.com/video/767082285?h=e76c380db4
          text: Schau das Video
          data_ga_name: nasdaq
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 800
      - header: Erzielen Sie eine 5-mal schnellere Bereitstellungszeit
        description: |
          Hackerone verbesserte die Pipelinezeit, Bereitstellungsgeschwindigkeit und Entwicklereffizienz mit GitLab Ultimate.
        showcase_img:
          url: /nuxt-images/software-faster/hackerone-showcase.png
          alt: Person working on a computer with code - HackerOne
        logo_img:
          url: /nuxt-images/logos/hackerone-logo.png
          alt: HackerOne logo
        link:
          href: /customers/hackerone/
          text: Erfahren Sie mehr
          data_ga_name: hackerone
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1000
      - header: Release-Funktionen 144x schneller
        description: |
          Airbus Intelligence verbesserte seinen Workflow und seine Codequalität mit Single Application CI.
        showcase_img:
          url: /nuxt-images/software-faster/airbus-showcase.png
          alt: Airplane wing on flight - Airbus
        logo_img:
          url: /nuxt-images/software-faster/airbus-logo.png
          alt: Airbus logo
        link:
          href: /customers/airbus/
          text: Lesen Sie ihre Geschichte
          data_ga_name: airbus
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1200
  devsecops:
    header: Alle wesentlichen DevSecOps-Tools in einer umfassenden Plattform
    link:
      text: Erfahren Sie mehr
      data_ga_name: platform
      data_ga_location: body
    cards:
      - header: Bessere Einblicke
        description: End-to-End-Sichtbarkeit über den gesamten Lebenszyklus der Softwarebereitstellung.
        icon: case-study-alt
      - header: Grössere Effizienz
        description: Integrierte Unterstützung für Automatisierung und Integrationen mit Drittanbieterdiensten.
        icon: principles
      - header: Verbesserte Zusammenarbeit
        description: Ein Workflow, der Entwickler-, Sicherheits- und Betriebsteams vereint.
        icon: roles
      - header: Schnellere Amortisationszeit
        description: Kontinuierliche Verbesserung durch beschleunigte Feedbackschleifen.
        icon: verification
  by_industry_case_studies:
    title: Erkunden Sie DevSecOps-Ressourcen
    charcoal_bg: true
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: Globale DevSecOps-Berichtsreihe 2023
        subtitle: Sehen Sie, was wir von mehr als 5.000 DevSecOps-Experten über den aktuellen Stand der Softwareentwicklung, Sicherheit und des Betriebs gelernt haben.
        image:
          url: /nuxt-images/software-faster/devsecops-survey.svg
          alt: devsecops survey icon
        button:
          href: /developer-survey/
          text: Lesen Sie den Bericht
          data_ga_name: devsecops survey
          data_ga_location: body
        icon:
          name: doc-pencil-alt
          alt: Play Circle Icon
      - title: 'Trittfrequenz ist alles: 10x Engineering-Organisationen für 10x Ingenieure.'
        subtitle: Sid Sijbrandij, CEO und Mitbegründer von GitLab, über die Bedeutung der Kadenz in Engineering-Organisationen.
        image:
          url: /nuxt-images/blogimages/athlinks_running.jpg
          alt: picture of people running marathon
        button:
          href: /blog/2022/11/03/cadence-is-everything-10x-engineering-organizations-for-10x-engineers/
          text: Lesen Sie den Blog
          data_ga_name: read the blog
          data_ga_location: body
        icon:
          name: blog
          alt: Play Circle Icon
