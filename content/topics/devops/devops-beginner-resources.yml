---
  title: Start your DevOps journey with these 11 helpful resources
  description: DevOps platform
  partenttopic: devops
  twitter_image: /nuxt-images/open-graph/gitlab-blog-cover.png
  date_published: 2022-03-15
  date_modified: 2023-04-11
  topics_header:
    data:
      title:  Start your DevOps journey with these 11 helpful resources
      block:
        - metadata:
            id_tag: what-is-devops
          text: |
              Here are the blogs, videos, webcasts, and more to help you get started with DevOps.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Devops
      href: /topics/devops/
      data_ga_name: devops
      data_ga_location: breadcrumb
    - title: DevOps Beginner Resources
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: How to begin your DevOps journey
          href: "#how-to-begin-your-dev-ops-journey"
          data_ga_name: how to begin your dev ops journey
          data_ga_location: side-navigation
          variant: primary
        - text: Track projects with epics and issues
          href: "#track-projects-with-epics-and-issues"
          data_ga_name: track projects with epics and issues
          data_ga_location: side-navigation
        - text: GitLab's guide to CI/CD for beginners
          href: "#git-lab-s-guide-to-ci-cd-for-beginners"
          data_ga_name: git lab s guide to ci cd for beginners
          data_ga_location: side-navigation
        - text: Your guide to learning about Git
          href: "#your-guide-to-learning-about-git"
          data_ga_name: your guide to learning about git
          data_ga_location: side-navigation
        - text: Understanding GitOps
          href: "#understanding-git-ops"
          data_ga_name: understanding-git-ops
          data_ga_location: side-navigation
        - text: Understanding DevSecOps
          href: "#understanding-dev-sec-ops"
          data_ga_name: understanding dev sec ops
          data_ga_location: side-navigation
        - text: How to be a stand-out DevOps team
          href: "#how-to-be-a-stand-out-dev-ops-team"
          data_ga_name: how to be a stand out dev ops team
          data_ga_location: side-navigation
        - text: How documentation can unify projects and team efforts
          href: "#how-documentation-can-unify-projects-and-team-efforts"
          data_ga_name: how documentation can unify projects and team efforts
          data_ga_location: side-navigation
    hyperlinks:
      text: "More on this topic"
      data:
        - text: Create the ideal DevOps team structure
          href: /topics/devops/build-a-devops-team/
          data_ga_location: header
          variant: tertiary
          icon: true
          data_ga_name: Create the ideal DevOps team structure
        - text: Continuous integration in DevOps
          href: /topics/ci-cd/benefits-continuous-integration/
          data_ga_location: header
          variant: tertiary
          icon: true
          data_ga_name: Continuous integration in DevOps
        - text: Understand continuous integration and delivery
          href: /topics/ci-cd/
          data_ga_location: header
          variant: tertiary
          icon: true
          data_ga_name: Understand continuous integration and delivery
    content:
      - name: topics-copy-block
        data:
          column_size: 10
          blocks:
            - text: |
                Since there are a lot of tools and terms to master, getting started in DevOps
                can be challenging. We've compiled a list of 11 useful and practical resources
                to help you quickly get up to speed.ere are many benefits of using an end-to-end DevOps platform, we're focusing here on two major gains: visibility and actionability.
      - name: topics-copy-block
        data:
          column_size: 10
          header: New to DevOps? Here's what you need to know
          blocks:
            - text: |
                If you're new to a DevOps team or consider yourself a DevOps beginner, we have a [guide that will help you get off the ground](https://learn.gitlab.com/beginners-guide-devops/guide-to-devops){data-ga-name="guide that will help" data-ga-location="body"}. This will help you understand what DevOps is all about, what terms, methodologies, and technologies you need to understand, and why collaboration is a key tenet that you will need to adopt. This guide also features an example of how DevOps is changing the game for one large financial investment bank. And it offers information on how working in DevOps can affect your career.
      - name: topics-copy-block
        data:
          column_size: 10
          header: How to begin your DevOps journey
          blocks:
            - text: |
                You've started working in DevOps or you want to be in the DevOps field, so you've begun a journey into learning new skills and adopting a new mindset. Here we walk you through [how to take the first steps](/blog/2022/01/13/how-to-begin-your-devops-journey/){data-ga-name="how to take the first steps" data-ga-location="body"} on this exciting new path.
      - name: topics-copy-block
        data:
          column_size: 10
          header: Track projects with epics and issues
          blocks:
            - text: |
                In a DevOps platform, users are better able to communicate, plan work, and collaborate by using epics and issues. [Epics](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="Epics" data-ga-location="body"} are an overview of a project, idea, or workflow. Issues are used to organize and list out what needs to be done to complete the larger goal, to track tasks and work status, or work on code implementations.


                For instance, if managers want an overview of how multiple projects, programs, or products are progressing, they can get that kind of visibility by checking an epic, which will give them a high-level rollup view of what is being worked on, what has been completed, and what is on schedule or delayed.


                Users can call up an epic to quickly see what's been accomplished and what is still under way, and then they can dig deeper into sub-epics and related issues for more information. [Issues](https://docs.gitlab.com/ee/user/project/issues/){data-ga-name="Issues" data-ga-location="body"} offer details about implementation of specific goals, trace collaboration on that topic, and show which parts of the initiative team members are taking on. Users also can see whether due dates have been met or have passed. Issues can be used to reassign pieces of work, give updates, make comments or suggestions, and see how the nuts and bolts are being created and moved around.
      - name: topics-copy-block
        data:
          column_size: 10
          header: GitLab's guide to CI/CD for beginners
          blocks:
            - text: |
                Continuous integration and [continuous delivery](/direction/release/continuous_delivery/#continuous-delivery-vs-deployment){data-ga-name="continuous delivery" data-ga-location="body"} (known as CI/CD) are the cornerstones of DevOps. [Here's what you need to know](/blog/2020/07/06/beginner-guide-ci-cd/){data-ga-name="Here's what you need to know" data-ga-location="body"} about CI/CD for beginners. And here's a [video tutorial](https://www.youtube.com/watch?v=DWb1HNmbmeM) that will help you, too.
      - name: topics-copy-block
        data:
          column_size: 10
          header: Your guide to learning about Git
          blocks:
            - text: |
                Whatever software you develop and whichever languages you use, you'll soon run into Git, a source code management system that helps programmers work collaboratively. Brendan O'Leary walks you through [what you need to know](/blog/2020/04/13/beginner-git-guide/){data-ga-name="Here's what you need to know" data-ga-location="body"}.
      - name: topics-copy-block
        data:
          column_size: 10
          header: Understanding GitOps
          blocks:
            - text: |
                [GitOps is an important operational framework](/topics/gitops/){data-ga-name="GitOps is an important operational framework" data-ga-location="body"} in DevOps, giving you a way to take best practices, like version control, compliance methodologies and CI/CD, and apply them to infrastructure automation and application deployment.


                To understand even more about GitOps and what it can do for your DevOps team, [check out this webcast](/why/gitops-infrastructure-automation/){data-ga-name="check out this webcast" data-ga-location="body"} of a panel discussion with pros from Weaveworks, HashiCorp, Red Hat, and GitLab talking about the future of infrastructure automation.
      - name: topics-copy-block
        data:
          column_size: 10
          header: Understanding DevSecOps
          blocks:
            - text: |
                The [practice of DevSecOps](/topics/devsecops/){data-ga-name="practice of DevSecOps" data-ga-location="body"} - or development, security, and operations - focuses on integrating security into the DevOps lifecycle. It's an approach to culture, automation, and platform design that makes it a shared responsibility, among everyone on the team, to create code with security in mind. By factoring in security this way, it increases efficiency and deployment speed, while also preventing, catching and solving bugs and compliance issues before code goes into production.


                For more information on DevSecOps, check out these [three best practices](/topics/devsecops/three-steps-to-better-devsecops/){data-ga-name="three best practices" data-ga-location="body"} for implementing better DevSecOps. And for [information on why developer-first security is important](/topics/devsecops/what-is-developer-first-security/){data-ga-name="information on why developer-first security is important" data-ga-location="body"}, here's more guidance for you.


                Want to know more about how to shift left? [This webcast](/webcast/wishes-to-workflows/){data-ga-name="This webcast" data-ga-location="body"} will help you understand how to make it happen.
      - name: topics-copy-block
        data:
          column_size: 10
          header: How to be a stand-out DevOps team
          blocks:
            - text: |
                There are several things you, and your teammates, can do to [make your DevOps team elite performers](/blog/2021/10/26/how-to-make-your-devops-team-elite-performers/){data-ga-name="make your DevOps team elite performers" data-ga-location="body"}. There's a big difference between being an elite performer and low performers, affecting your speed to deployment, efficiency and your corporate agility. Check out the advantages, as well as tips on how to get there.
      - name: topics-copy-block
        data:
          column_size: 10
          header: How documentation can unify projects and team efforts
          blocks:
            - text: |
                If you're looking to figure out how to unify efforts between projects and DevOps teams, and to share specialized knowledge and guidance, you need to learn about documentation. This blog will walk you through [what documentation is all about](/blog/2022/01/11/16-ways-to-get-the-most-out-of-software-documentation/){data-ga-name="what documentation is all about" data-ga-location="body"} and what it can do for your DevOps efforts.
