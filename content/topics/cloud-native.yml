---
  title: What is Cloud native?
  description:  Learn about cloud native applications that leverage technologies like containers, Kubernetes, and microservices to run at unprecedented scale and stability.
  topic_name: Cloud native
  icon: cloud-server
  date_published: 2022-07-06
  date_modified: 2023-04-10
  topics_header:
    data:
      title: What is cloud native?
      block:
          - text: |
                Cloud native is an approach that uses technologies such as containers, Kubernetes, immutable infrastructure, and microservices to develop scalable applications that are built to run in the cloud.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Cloud Native
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: Cloud native explained
          href: "#cloud-native-explained"
          data_ga_name: cloud native explained
          data_ga_location: side-navigation
          variant: primary
        - text: Essential elements of a cloud-native architecture
          href: "#essential-elements-of-a-cloud-native-architecture"
          data_ga_name: essential elements of a cloud nativearchitecture
          data_ga_location: side-navigation
          variant: primary
        - text: Cloud-native application benefits
          href: "#cloud-native-application-benefits"
          data_ga_name: cloud native application benefits
          data_ga_location: side-navigation
          variant: primary
        - text: Challenges of cloud-native application development
          href: "#challenges-of-cloud-native-application-development"
          data_ga_name: challenges of cloud native application development
          data_ga_location: side-navigation
          variant: primary
        - text: Why should an enterprise be cloud native?
          href: "#why-should-an-enterprise-be-cloud-native"
          data_ga_name: challenges of cloud native application development
          data_ga_location: side-navigation
          variant: primary
        - text: The building blocks of cloud-native architecture
          href: "#the-building-blocks-of-cloud-native-architecture"
          data_ga_name: "the building blocks of cloud native architecture"
          data_ga_location: side-navigation
          variant: primary
        - text: How to build cloud-native applications
          href: "#how-to-build-cloud-native-applications"
          data_ga_name: "how to build cloud native applications"
          data_ga_location: side-navigation
          variant: primary
        - text: Cloud native for Business
          href: "#cloud-native-for-business"
          data_ga_name: "cloud native for business"
          data_ga_location: side-navigation
          variant: primary
    hyperlinks:
      text: ''
      data: []
    content:
      - name: 'topics-copy-block'
        data:
          header: Cloud native explained
          column_size: 10
          blocks:
              - text: |
                  Cloud native is more than simply taking existing apps and running them in the cloud.

                  Cloud native is a term used to describe software that is built to run in a cloud computing environment. These applications are designed to be scalable, highly available, and easy to manage. By contrast, traditional solutions are often designed for on-premises environments and then adapted for a cloud environment. This can lead to sub-optimal performance and increased complexity.

                  The Cloud Native Computing Foundation (CNCF), an open source software organization focused on promoting the cloud-based app building and deployment approach, [defines cloud native technologies](https://github.com/cncf/toc/blob/main/DEFINITION.md) as those that “empower organizations to build and run scalable applications in modern, dynamic environments such as public, private, and hybrid clouds.”

                  As enterprises move more of their workloads to the cloud, they are increasingly looking for solutions that are cloud native. Cloud-native technologies are designed from the ground up to take advantage of the unique characteristics of cloud technologies, such as scalability, elasticity, and agility.
      - name: 'topics-copy-block'
        data:
          header: Essential elements of a cloud-native architecture
          column_size: 10
          blocks:
              - text: |
                  There are three key elements to any cloud-native architecture:

                  1. **It is containerized**. Each part (applications, processes, etc.) is packaged in its own container. This facilitates reproducibility, transparency, and resource isolation.
                  2. **It is dynamically managed**. Containers are actively orchestrated to optimize resource utilization.
                  3. **It is microservices-oriented**. Applications are segmented into microservices, which significantly increases their overall agility and maintainability.
      - name: 'topics-copy-block'
        data:
          header: Cloud-native application benefits
          column_size: 10
          blocks:
              - text: |
                  Taking full advantage of the power of the cloud computing model and container orchestration, cloud native is an innovative way to build and run applications. Cloud-native applications are built to run in the cloud, moving the focus away from machines to the actual service.

                  Because cloud-native applications are architectured using [microservices](/topics/microservices/) instead of a monolithic application structure, they rely on containers to package the application’s libraries and processes for deployment. Microservices allow developers to build deployable apps that are composed as individual modules focused on performing one specific service. This decentralization makes for a more resilient environment by limiting the potential of full application failure due to an isolated problem.

                  Container orchestration tools like [Kubernetes](/solutions/kubernetes/) allow developers to coordinate the way in which an application’s containers will function, including scaling and deployment.

                  Using a cloud-native approach to build your applications has a number of tangible benefits:

                  *  Saves money by monitoring and scaling application resources through cloud orchestration, i.e., container schedulers
                  *  Allows teams to ship updates and drive value for customers more quickly
                  *  Aligns operations with business goals
                  *  Reduces time spent on maintenance, meaning more time can be spent focusing on business goals
      - name: 'topics-copy-block'
        data:
          header: Challenges of cloud-native application development
          column_size: 10
          blocks:
              - text: |
                  The cloud-native movement has brought new challenges for developers, ops teams, and organizations as a whole. Common challenges include:

                  *  Managing multiple versions of software across different cloud providers
                  *  Scaling applications up and down quickly
                  *  Managing complexity as more services and components are added to the mix
                  *  Dealing with ephemeral infrastructure, which can make debugging and troubleshooting difficult
                  *  Ensuring efficient use of resources, as the pay-as-you-go model of the cloud can quickly get expensive
                  *  Making sure all components work together seamlessly

                  The key to cloud-native development is to use tools like Kubernetes, Docker containers, and Terraform to automate deployment, configuration management, and infrastructure provisioning. Organizations need to be aware of these challenges and have the necessary strategies and solutions in place to address them as they arise.
      - name: 'topics-copy-block'
        data:
          header: Why should an enterprise be cloud native?
          column_size: 10
          blocks:
              - text: |
                  Cloud-native applications are designed to be more resilient and scalable than traditional applications. This is because they use cloud-based services to store data, run applications, and access resources. By transitioning to cloud-native applications, an enterprise can improve its resilience and scalability. Cloud-native enterprises can quickly adapt to changing market conditions and customer demands while reducing their IT infrastructure costs.

                  In addition to increased security and compliance capabilities and better visibility into the applications and services that make up the enterprise, this approach can also save money by reducing the number of servers and software required.
      - name: 'topics-copy-block'
        data:
          column_size: 10
          header: The building blocks of cloud-native architecture
          blocks:
              - text: |
                  ### Containers

                  [Containers](/blog/2017/11/30/containers-kubernetes-basics/) are an [alternative way to package applications](https://searchitoperations.techtarget.com/tip/What-are-containers-and-how-do-they-work) versus building for virtual machines (VMs) or physical servers directly. Everything needed to run an application (such as code, system libraries, and settings) is included in a container image — a lightweight, standalone, executable package of software. Containers can run inside of a VM or on a physical server. Containers hold an application’s libraries and processes, but don't include an operating system, making them lightweight. In the end, fewer servers are needed to run multiple instances of an application, which reduces cost and makes them easier to scale. Some other [benefits of containers](https://tsa.com/top-5-benefits-of-containerization/) include faster deployment, better portability and scalability, and improved security.


                  ### Orchestrators

                  Once the containers are set, an orchestrator is needed to get them running. Container orchestrators direct how and where containers run, fix any that go down, and determine if more are needed. When it comes to container orchestrators, also known as schedulers, Kubernetes is the clear-cut [market winner](/blog/2018/08/02/top-five-cloud-trends/).

                  ### Microservices

                  The last main component of cloud-native computing is microservices. To make apps run more smoothly, they can be broken down into smaller parts, or microservices, to make them easier to scale based on load. Microservices infrastructure also makes it easier — and faster — for engineers to develop an app. Smaller teams can be formed and assigned to take ownership of individual components of the app’s development, allowing engineers to code without potentially impacting another part of the project.

                  While public cloud platforms like AWS offer the opportunity to build and deploy applications easily, there are times when it makes sense to build your own infrastructure. A private or hybrid cloud solution is generally needed when sensitive data is processed within an application or industry regulations call for increased controls and security.
      - name: 'topics-copy-block'
        data:
          column_size: 10
          header: How to build cloud-native applications
          blocks:
              - text: |
                  Developing cloud-native apps requires the incorporation of several tools for a successful deployment. In their definition of cloud native, the CNCF notes that cloud-native techniques, when combined with robust automation, allow engineers to make high-impact changes frequently and predictably with minimal effort.

                  Cloud native app development requires a shift to [DevOps](/topics/devops/) practices. This means development and operations teams will work much more collaboratively, leading to a faster and smoother production process. A DevOps approach efficiently streamlines the multiple elements needed to get an app up and running in the cloud.

                  When transitioning to cloud-native applications, your team will see a large increase in the number of projects that need to be managed because of the required use of microservices. The surge in project volume calls for consistent and efficient application lifecycle management — this is where GitLab comes in.

                  GitLab is a [DevOps platform](/topics/devops/) delivered as a single application. From issue tracking and source code management to CI/CD and monitoring, having it all in one place simplifies toolchain complexity and speeds up cycle times. With a [built-in container registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html) and [Kubernetes integration](https://docs.gitlab.com/ee/user/project/clusters/index.html), GitLab makes it easier than ever to get started with containers and cloud-native development.
                video:
                  video_url: https://www.youtube.com/embed/jc5cY3LoOOI?start=98
      - name: 'topics-copy-block'
        data:
          column_size: 10
          header: Cloud native for Business
          blocks:
              - text: |
                  Cloud-native applications use containers, microservices architecture, and container orchestration like Kubernetes. GitLab is designed for cloud-native applications with tight Kubernetes integration.

                  Businesses are shifting from traditional deployment models to cloud-native applications in order to gain speed, reliability, and scale.

                  Learn more about how GitLab can power your cloud-native development.
                video:
                  video_url: https://www.youtube.com/embed/wtaOQY_ITvQ
      - name: topics-cta
        data:
          title: Start your cloud native transformation
          text: |
            Hear how Ask Media Group migrated from on-prem servers to the AWS cloud with GitLab tools and integrations. Join us and learn from their experience.
          column_size: 10
          cta_one:
            text: Save your spot
            link: /webcast/cloud-native-transformation/
            data_ga_name: Save your spot
            data_ga_location: body
  components:
    - name: solutions-resource-cards
      data:
        title: Related Resources
        column_size: 4
        grouped: true
        cards:
          - icon:
              name: webcast
              variant: marketing
              alt: Webcast Icon
            event_type: "Video"
            header: Cloud Native Webinar
            link_text: "Watch now"
            image: "/nuxt-images/topics/cloud-native-webinar.jpeg"
            href: https://www.youtube.com/embed/jc5cY3LoOOI
            data_ga_name: Cloud native webinar
            data_ga_location: resource cards
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: "Blog"
            header: Cloud Native Development with GitLab
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
            href: /blog/2017/04/18/cloud-native-demo/
            data_ga_name: Cloud Native Development with GitLab
            data_ga_location: resource cards
          - icon:
              name: articles
              variant: marketing
              alt: Articles Icon
            event_type: "Blog"
            header: Create a CI/CD Pipeline with Auto Deploy
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-cicd.png"
            href: /blog/2017/09/21/how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm/
            data_ga_name: Create a CI/CD Pipeline with Auto Deploy
            data_ga_location: resource cards
          - icon:
              name: articles
              variant: marketing
              alt: Articles Icon
            event_type: "Doc"
            header: Auto DevOps Documentation
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-gitlab.png"
            href: https://docs.gitlab.com/ee/topics/autodevops/index.html
            data_ga_name: Auto DevOps Documentation
            data_ga_location: resource cards
