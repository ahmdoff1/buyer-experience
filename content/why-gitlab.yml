---
  title: Why GitLab?
  description: "Don't only take our word for it! GitLab is the only comprehensive DevOps platform being delivered as a single application. Learn more here!"
  hero:
    header: Industry leaders choose GitLab
    description: GitLab is the only place where enterprises build mission‑critical software.
    primary_button:
      text: Try Ultimate for free
      href: https://gitlab.com/-/trials/new?_gl=1%2A1lt5wks%2A_ga%2AMTc4NzA4MjI4Ni4xNjc1OTYwMzk0%2A_ga_ENFH3X7M5Y%2AMTY4MTE0OTk0MC4xMjkuMS4xNjgxMTUwMTMwLjAuMC4w&glm_content=default-saas-trial&glm_source=about.gitlab.com%2Fpricing%2F
    video:
      title: What is GitLab?
      url: https://player.vimeo.com/video/799236905?h=4eee39a447&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
      image: /nuxt-images/solutions/landing/solutions_video_thumbnail.png
  categories_block:
    categories:
      - title: Developers
        bullets:
          - title: Single application
            description: GitLab brings all DevSecOps capabilities into one application with a unified data store so everything is all in one place.
          - title: Enhanced productivity
            description: GitLab’s single application delivers a superior user experience, which improves cycle time and helps prevent context switching.
          - title: Better automation, where it really counts
            description: GitLab’s automation tools are more reliable and feature rich, helping remove cognitive load and unnecessary grunt work.
        customer:
          logo: /nuxt-images/logos/deakin-university-logo.svg
          text: See how Deakin University reduced manual tasks by 60% with GitLab.
          href: /customers/deakin-university/
          dataGaName: deakin
        cta:
          icon: time-is-money
          title: Does your DevSecOps platform automate tasks that improves team efficiency and reduces developer coding time?
          text: Automation features are a crucial piece of a comprehensive DevSecOps platform that ensure efficiency, scale, security, and compliance, in addition to creating code.
          button:
            text: Calculate your potential savings with our ROI tool
            href: /calculator/roi/
            dataGaName: roi calculator
      - title: Security
        bullets:
          - title: Security is built in, not bolted on
            description: GitLab’s security capabilities – such as DAST,  fuzz testing, container scanning, and API screening – are integrated end-to-end.
          - title: Compliance and precise policy management
            description: GitLab offers a comprehensive governance solution allowing for separation of duties between teams. GitLab’s policy editor allows customized approval rules tailored to each organization’s compliance requirements, reducing risk.
          - title: Security automation
            description: GitLab’s advanced automation tools enable velocity with guardrails, ensuring code is automatically scanned for vulnerabilities.
        customer:
          logo: /nuxt-images/logos/hackerone-logo.png
          text: See how HackerOne’s engineering team used automation with GitLab to save manual cycle time and create faster security scanning, saving an additional hour per deploy on testing.
          href: /customers/hackerone/
          dataGaName: hackerone
        cta:
          icon: shield-check-light
          title: Is your platform able to integrate security throughout the Software Delivery Life Cycle?
          text: Integrating security at every step reduces the need for additional integrations and minimizes the risk of failure
          button:
            text: Learn more about our commitment to information security
            href: /security/
            dataGaName: security
      - title: Operations
        bullets:
          - title: Scale Enterprise workloads
            description: GitLab easily supports the Enterprise at any scale with the ability to manage and upgrade with nearly zero downtime.
          - title: Unparalleled metrics visibility
            description: GitLab's unified data store provides analytics for the entire software development lifecycle in one place, eliminating the need for additional product integrations.
          - title: No cloud lock in
            description: GitLab is not commercially tied to a single cloud provider, eliminating the risk of vendor lock-in.
        customer:
          logo: /nuxt-images/home/logo_iron_mountain_mono.svg
          text: See how Iron Mountain reduced infrastructure management costs and securely increased production velocity with GitLab, which saved more than $150,000 per year and cut the number of on-premises virtual machines by nearly half.
          href: /customers/iron-mountain/
          dataGaName: iron mountain
        cta:
          icon: time-is-money
          title: 'Total Cost of Ownership: Can your DevSecOps platform scale effectively without incurring excessive costs?'
          text: The additional tools that some providers require can quickly become exorbitantly costly – in both management and maintenance overhead and money –  as your organization grows
          button:
            text: Talk to an expert
            href: /sales/
            dataGaName: sales
  badges:
    header: Loved by developers. <br> Trusted by enterprises.
    description: GitLab ranks as a G2 Leader across DevOps categories.
    badges:
    - src: /nuxt-images/why/badge1.svg
      alt: G2 Best Results - Spring 2023
    - src: /nuxt-images/why/badge2.svg
      alt: G2 Enterprise Leader - Spring 2023
    - src: /nuxt-images/why/badge3.svg
      alt: G2 Easiest to use - Spring 2023
    - src: /nuxt-images/why/badge4.svg
      alt: G2 Best Relationship Enterprise - Spring 2022
    - src: /nuxt-images/why/badge5.svg
      alt: G2 Highest user adoption - Spring 2022
    - src: /nuxt-images/why/badge6.svg
      alt: G2 Most implementable - Spring 2022
  pricing:
    header: Start delivering software faster
    tiers:
      - name: Premium
        purchaseUrl: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities&_gl=1*7pkvar*_ga*MjE0NDM3MDYyNi4xNjY1NjI1NDM4*_ga_ENFH3X7M5Y*MTY4MzU4MTkzNS4zMDguMS4xNjgzNTgzNzY1LjAuMC4w
        learnMoreUrl: /pricing/premium/
        benefits:
          - Code Ownership and Protected Branches
          - Merge Requests with Approval Rules
          - Enterprise Agile Planning
      - name: Ultimate
        purchaseUrl: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities&_gl=1*cwydal*_ga*MjE0NDM3MDYyNi4xNjY1NjI1NDM4*_ga_ENFH3X7M5Y*MTY4MzU4MTkzNS4zMDguMS4xNjgzNTgzNzc4LjAuMC4w
        learnMoreUrl: /pricing/ultimate/
        benefits:
          - Dynamic Application Security Testing
          - Security Dashboards
          - Vulnerability Management
          - Dependency Scanning
          - Container Scanning
    footnote: Not ready for [Premium](/pricing/premium/){data-ga-name="premium note" data-ga-location="pricing tier blocks"} or [Ultimate](/pricing/ultimate/){data-ga-name="ultimate note" data-ga-location="pricing tier blocks"} yet? You can always sign up for our [Free Tier](https://gitlab.com/-/trials/new?_gl=1%2A1lt5wks%2A_ga%2AMTc4NzA4MjI4Ni4xNjc1OTYwMzk0%2A_ga_ENFH3X7M5Y%2AMTY4MTE0OTk0MC4xMjkuMS4xNjgxMTUwMTMwLjAuMC4w&glm_content=default-saas-trial&glm_source=about.gitlab.com%2Fpricing%2F){data-ga-name="free note" data-ga-location="pricing tier blocks"}.
