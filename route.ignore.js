// File used to prevent generating routes for content files which do not describe pages
// This is done to avoid errors during yarn generate

// eslint-disable-next-line import/no-default-export
export default [
  'content/free-trial-cta.yml',
  'content/next-step.yml',
  'content/navigation.yml',
  'content/footer.yml',
  'content/apps.yml',
  'content/synced/categories.yml',
  'content/synced/features.yml',
  'content/competition/base_template.yml',
  'content/competition/competitor_template.yml',
  'content/customers/template.yml',
  'content/customers/base_template.yml',
  // Sunsetted pages we keep to turn on when needed
  'content/livestream.yml',
  'content/partners/new.yml',
  // Landing pages:
  // These pages have children pages already migrated but an error pops up in the console as nuxt expect the parent to exist
  // These exceptions should be removed when migrating the landing page.
  'content/topics/devsecops.yml',
  'content/topics/devsecops',
  'content/company/culture',
  'content/company/team',
  'content/pricing/self-managed',
  'content/calculator',
  'content/events',
  'content/synced',
  'content/features/related-data.yml',
  'content/features/features-cta-section.yml',
  'content/resources/resources-list.yml',
  'content/customers/all/case-studies-list.yml',
  'content/de-de',
  'content/de-de/index.yml',
  'content/de-de/features',
  'content/de-de/features/related-data.yml',
  'content/de-de/features/features-cta-section.yml',
  'content/ja-jp',
  'content/ja-jp/index.yml',
  'content/ja-jp/features',
  'content/ja-jp/features/related-data.yml',
  'content/ja-jp/features/features-cta-section.yml',
  'content/fr-fr',
  'content/fr-fr/index.yml',
  'content/fr-fr/features',
  'content/fr-fr/features/related-data.yml',
  'content/fr-fr/features/features-cta-section.yml',
  'content/developer-survey/previous',
];
