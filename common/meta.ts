import {
  MetaPropertyCharset,
  MetaPropertyEquiv,
  MetaPropertyMicrodata,
  MetaPropertyName,
  MetaPropertyProperty,
  MetaInfo,
} from 'vue-meta/types/vue-meta';
import { NuxtI18nMeta } from '@nuxtjs/i18n/types/vue';
import { createSchemaPage, createSchemaFaq } from './craftSchema';
import {
  DEFAULT_META_DESCRIPTION,
  DEFAULT_OPENGRAPH_IMAGE,
  DEFAULT_SCHEMA_ORG,
  META_NAME,
  SITE_URL,
  TWITTER_CARD_CONTENT,
  TWITTER_CREATOR_CONTENT,
  TWITTER_SITE_CONTENT,
} from '~/common/constants';

function buildSchemaOrg(schemaOrg: string, description: string) {
  const schemaScript = {
    hid: 'schemaOrg',
    innerHTML: '',
    type: 'application/ld+json',
  };

  if (schemaOrg) {
    schemaScript.innerHTML = schemaOrg;
  } else {
    const schema = {
      ...DEFAULT_SCHEMA_ORG,
      description: description || DEFAULT_SCHEMA_ORG.description,
    };

    schemaScript.innerHTML = JSON.stringify(schema);
  }

  return schemaScript;
}

function buildTopicsSchema({
  title,
  description,
  path,
  topicName,
  topicsHeader,
  dateModified,
  datePublished,
}: {
  title: string;
  description: string;
  path: string;
  datePublished?: string;
  dateModified?: string;
  topicName?: string;
  topicsHeader?: any;
}) {
  const schemaValues = {
    title,
    description,
    datePublished,
    dateModified: dateModified || datePublished,
    url: `${SITE_URL}${path}`,
    articleSection: topicName,
    timeRequired: topicsHeader?.data.read_time || '',
    image: '/nuxt-images/topics/gitops-topic.png',
  };

  return {
    hid: 'schemaTopics',
    json: createSchemaPage(schemaValues),
    type: 'application/ld+json',
  };
}

/* eslint-disable babel/camelcase */
export function getPageMetadata(
  {
    description = DEFAULT_META_DESCRIPTION,
    title,
    image_title: imageTitle,
    twitter_image: twitterImage,
    image_alt: imageAlt,
    schema_faq: schemaFaq,
    schema_org: schemaOrg,
    topic_name: topicName,
    topics_header: topicsHeader,
    date_published: datePublished,
    date_modified: dateModified,
  }: {
    description: string;
    title: string;
    image_title: string;
    twitter_image: string;
    image_alt: string;
    schema_faq: [];
    schema_org: string;
    topic_name?: string;
    time_required?: string;
    topics_header?: any;
    date_published?: string;
    date_modified: string;
  },
  path: string,
  nuxtI18nHead?: NuxtI18nMeta,
): MetaInfo {
  let meta: (
    | MetaPropertyCharset
    | MetaPropertyEquiv
    | MetaPropertyName
    | MetaPropertyMicrodata
    | MetaPropertyProperty
    | any
  )[] = [];
  const script: any = [];

  meta.push({
    hid: META_NAME.description,
    name: META_NAME.description,
    content: description,
  });
  meta.push({
    hid: META_NAME.twitterDescription,
    name: META_NAME.twitterDescription,
    content: description,
  });

  meta.push({
    hid: META_NAME.ogDescription,
    property: META_NAME.ogDescription,
    content: description,
  });

  meta.push({
    hid: META_NAME.ogTitle,
    property: META_NAME.ogTitle,
    content: title,
  });

  meta.push({
    hid: META_NAME.twitterCreator,
    name: META_NAME.twitterCreator,
    content: TWITTER_CREATOR_CONTENT,
  });

  meta.push({
    hid: META_NAME.twitterSite,
    name: META_NAME.twitterSite,
    content: TWITTER_SITE_CONTENT,
  });

  meta.push({
    hid: META_NAME.twitterCard,
    name: META_NAME.twitterCard,
    content: TWITTER_CARD_CONTENT,
  });

  meta.push({
    hid: META_NAME.twitterAltImage,
    name: META_NAME.twitterAltImage,
    content: imageAlt,
  });

  meta.push({
    hid: META_NAME.ogImageAlt,
    property: META_NAME.ogImageAlt,
    content: imageAlt,
  });

  if (path) {
    meta.push({
      hid: META_NAME.ogUrl,
      name: META_NAME.ogUrl,
      content: `${SITE_URL}${path}`,
    });
  }

  if (title) {
    meta.push({
      hid: META_NAME.ogType,
      property: META_NAME.ogType,
      content: META_NAME.article,
    });
    meta.push({
      hid: META_NAME.twitterTitle,
      name: META_NAME.twitterTitle,
      content: title,
    });
  } else {
    meta.push({
      hid: META_NAME.ogType,
      content: META_NAME.website,
      property: META_NAME.ogType,
    });
  }

  if (imageTitle) {
    meta.push({
      hid: META_NAME.twitterImage,
      name: META_NAME.twitterImage,
      content: `${SITE_URL}${imageTitle}`,
    });
    meta.push({
      hid: META_NAME.ogImage,
      property: META_NAME.ogImage,
      content: `${SITE_URL}${imageTitle}`,
    });
  } else if (twitterImage) {
    meta.push({
      hid: META_NAME.twitterImage,
      name: META_NAME.twitterImage,
      content: `${SITE_URL}${twitterImage}`,
    });
    meta.push({
      hid: META_NAME.ogImage,
      property: META_NAME.ogImage,
      content: `${SITE_URL}${twitterImage}`,
    });
  } else {
    meta.push({
      hid: META_NAME.twitterImage,
      name: META_NAME.twitterImage,
      content: `${SITE_URL}${DEFAULT_OPENGRAPH_IMAGE}`,
    });
    meta.push({
      hid: META_NAME.ogImage,
      property: META_NAME.ogImage,
      content: `${SITE_URL}${DEFAULT_OPENGRAPH_IMAGE}`,
    });
  }

  if (schemaFaq) {
    script.push({
      hid: 'schemaFaq',
      json: createSchemaFaq(schemaFaq),
      type: 'application/ld+json',
    });
  }

  if (path.includes('topics')) {
    script.push(
      buildTopicsSchema({
        title,
        description,
        path,
        topicName,
        topicsHeader,
        dateModified,
        datePublished,
      }),
    );
  }

  script.push(buildSchemaOrg(schemaOrg, description));

  if (nuxtI18nHead) {
    const absHref = nuxtI18nHead.link.map((link) => {
      const fullLink = { ...link };
      fullLink.href = `${SITE_URL}${fullLink.href}/`;
      return fullLink;
    });
    meta = [...meta, ...absHref];
  }

  return {
    title: `${title} | GitLab`,
    meta,
    script,
    __dangerouslyDisableSanitizers: ['script', 'innerHTML'],
  };
}
